import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Wrapper from "./components/wrapper/Wrapper";
import {ToastContainer} from "react-toastify";
import {Route, Switch} from "react-router";
import PageNotFound from "./components/pagenotfound/PageNotFound";
import {BrowserRouter} from "react-router-dom";
import Home from "./components/home/Home";
import * as constants from "./utils/Constants";
import * as globalcss from './Global.css';
import Signin from "./components/login/signin/Signin";
import GrainList from "./components/grainlist/GrainList";
import Signup from "./components/login/signup/Signup";

class App extends Component {
    render() {
        return (
            <Wrapper>
                <ToastContainer />
                <BrowserRouter>
                    <Switch>
                        <Route path={constants.NAVIGATION_HOME} exact component={Home}/>
                        <Route path={constants.NAVIGATION_GRAIN_LIST} exact component={GrainList}/>
                        <Route path={constants.NAVIGATION_SIGNIN} exact component={Signin}/>
                        <Route path={constants.NAVIGATION_SIGNUP} exact component={Signup}/>

                        <Route exact component={PageNotFound}/>
                    </Switch>
                </BrowserRouter>
            </Wrapper>

        );
    }
}

export default App;
