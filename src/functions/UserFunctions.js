import * as errorsFirebase from "../utils/FirebaseErrors";
import {FirebaseAuth, FirebaseRef} from '../utils/FirebaseConfigurator'
import * as utils from "../utils/Utils";
import User from "../utils/hoc/User";
import * as constants from "../utils/Constants";
import * as firebaseTables from "../utils/FirebaseTables";


export function signup (email, pw, successCallback, errorCallback) {
    if(!pw || !email) {
        //Firebase it seems break if you don't put a psw (at least at the first time)
        const fakeObjError = [];
        fakeObjError.code = errorsFirebase.LOGIN_INVALID_PASSWORD;
        errorCallback(fakeObjError);
        return;
    }

    FirebaseAuth().createUserWithEmailAndPassword(email, pw).then((createdUser)=>{
        if(createdUser && createdUser.user){
            createdUser.user.sendEmailVerification();
            //create the record:
            const user = {};
            user.email = email;
            user.uid = createdUser.user.uid;
            user.cluster = constants.USER_CLUSTER_NORMAL;

            FirebaseRef.collection(firebaseTables.TABLE_USERS).doc(user.uid).set( user ).then((docRef) =>{
                if(successCallback)successCallback(docRef);
            }).catch((error)=> {
                if(errorCallback)errorCallback(error);
                utils.logError(error)
            });
        }else{
            const fakeObjError = [];
            fakeObjError.code = errorsFirebase.LOGIN_USER_GENERIC;
            if(errorCallback)errorCallback(fakeObjError)
        }
    }).catch((error)=> {
        if(errorCallback)errorCallback(error)
        utils.logError(error)
    });
};

export function signin (email, pw, successCallback, errorCallback) {
    if(!pw) {
        //Firebase it seems break if you don't put a psw (at least at the first time)
        const fakeObjError = [];
        fakeObjError.code = errorsFirebase.LOGIN_INVALID_PASSWORD;
        errorCallback(fakeObjError);
        return;
    }
    return FirebaseAuth().signInWithEmailAndPassword(email, pw).then(function(user){
        if(!user.user.emailVerified){
            errorCallback({code:errorsFirebase.LOGIN_USER_UNCONFIRMED})
        }else{
            successCallback(user)
        }
    }).catch((error)=> {
        if(errorCallback)errorCallback(error)
        utils.logError(error)
    });
}

export function signout (callback) {
    return FirebaseAuth().signOut().then(callback);
}