import React from 'react';
import * as css from './LoginContainer.css';
import MainContainer from "../../maincontainer/MainContainer";
import Separator from "../../separator/Separator";
import {Col} from "react-flexbox-grid";

const LoginContainer = (props) => {
    return (
        <MainContainer>
            <div className={"loginContainer"}>
                <div className={"center"}>
                    <h1 className={"title"}>{props.title}</h1>
                    <h2 className={"subtitle"}>{props.subtitle}</h2>
                    <p className={"terms"}>{props.terms}</p>
                </div>
                <Separator width={"50%"} marginTop={"40px"}/>
                <div className={"child"}>
                    {props.children}
                </div>
            </div>
        </MainContainer>
    );
};

export default LoginContainer;
