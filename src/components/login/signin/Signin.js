import React, {Component} from 'react';
import LoginContainer from "../logincontainer/LoginContainer";
import * as strings from "../../../utils/Strings";
import Input from "../../input/Input";
import {Col, Row} from "react-flexbox-grid";
import AsyncButton from "../../asyncButton/AsyncButton";
import {Link} from "react-router-dom";
import * as constants from "../../../utils/Constants";

class Signin extends Component {
    constructor(args) {
        super(args);
        this.state = {
            email:null,
            loading:false,
            password:null,
            errors:{
                email:null,
                password:null,
                message:null
            }
        }
    }

    loginHandler = () =>{

    }

    render() {
        return (
            <LoginContainer title={strings.stringsSignin.TITLE} subtitle={strings.stringsSignin.SUBTITLE} terms={strings.stringsSignin.TERMS}>
                <div className={"signinContainer"}>
                    <Row>
                        <Col xs={12} lg={6} xl={3} className={"marginCenter"}>
                            <Row>
                                <Col xs={12} className={"marginTop20"}>
                                    <Input label={strings.stringsSignin.LBL_EMAIL}/>
                                </Col>
                                <Col xs={12} className={"marginTop20"}>
                                    <Input type={"password"} label={strings.stringsSignin.LBL_PASSWORD}/>
                                </Col>
                                <Col xs={12} className={"marginTop20 center"}>
                                    <Link to={constants.NAVIGATION_SIGNUP}>{strings.stringsSignin.LBL_GO_TO_SIGNUP}</Link>
                                </Col>
                                <Col xs={12} className={"marginTop20"}>
                                    <AsyncButton className={"defaultButton"}
                                                 loading={this.state.loading}
                                                 textButton={strings.stringsSignin.BTN_SUBMIT}
                                                 onClick={this.loginHandler}/>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </div>
            </LoginContainer>
        );
    }
}

export default Signin;