import React, {Component} from 'react';
import LoginContainer from "../logincontainer/LoginContainer";
import * as strings from "../../../utils/Strings";
import Input from "../../input/Input";
import {Col, Row} from "react-flexbox-grid";
import AsyncButton from "../../asyncButton/AsyncButton";
import * as constants from "../../../utils/Constants";
import {Link} from "react-router-dom";
import * as errorsFirebase from "../../../utils/FirebaseErrors";
import * as utils from "../../../utils/Utils";
import * as userFunctions from "../../../functions/UserFunctions";

class Signup extends Component {
    constructor(args) {
        super(args);
        this.state = {
            email:null,
            loading:false,
            psw1:null,
            psw2:null,
            errors:{
                email:null,
                password:null,
                message:null
            },
            successMessage:null
        }
    }

    signupHandler = () =>{
        if(this.checkError()){
            const tempState = utils.deepCopy(this.state);
            tempState.loading = true;
            this.setState(tempState,()=>{
                userFunctions.signup(tempState.email, tempState.psw1,this.successSignup,this.errorSignup);
            });
        }
    }

    successSignup = () =>{
        const tempState = JSON.parse(JSON.stringify(this.state));
        tempState.successMessage = strings.stringsSignup.MESSAGE_SUCCESS_SIGNUP;
        tempState.errors.message = null;
        tempState.loading = false;
        this.setState(tempState);
    }

    errorSignup = (error) =>{
        const tempState = JSON.parse(JSON.stringify(this.state));
        tempState.loading = false;
        switch(error.code){
            case errorsFirebase.LOGIN_BAD_FORMAT_EMAIL:
                tempState.errors.message = strings.stringsSignup.errors.ERROR_EMAIL;
                break;
            case errorsFirebase.LOGIN_INVALID_PASSWORD:
                tempState.errors.message = strings.stringsSignup.errors.ERROR_PASSWORD;
                break
            case errorsFirebase.CHANGE_PASSWORD_WEAK:
                tempState.errors.message = strings.stringsSignup.errors.ERROR_PASSWORD;
                break
            default:
                tempState.errors.message = error.message;
                break;
        }
        this.setState(tempState);
    }

    checkError = () =>{
        const tempState = utils.deepCopy(this.state);
        let check = true;
        for(let propertyName in tempState.errors) {
            tempState.errors[propertyName] = false;
        }
        tempState.errors.message = null;

        if(!tempState.email || tempState.email.trim() === "" || !utils.validateEmail(tempState.email)){
            tempState.errors.email = true;
            tempState.errors.message = strings.stringsSignup.errors.ERROR_EMAIL;
            check = false;
        }else if(!tempState.psw1 || tempState.psw1.trim() === "" || tempState.psw1.length < 6){
            tempState.errors.password = true;
            tempState.errors.message = strings.stringsSignup.errors.ERROR_PASSWORD;
            check = false;
        }else if(tempState.psw1 !== tempState.psw2){
            tempState.errors.password = true;
            tempState.errors.message = strings.stringsSignup.errors.ERROR_PASSWORD_MATCH;
            check = false;
        }

        this.setState(tempState);
        return check;
    }

    render() {
        return (
            <LoginContainer title={strings.stringsSignup.TITLE} subtitle={strings.stringsSignup.SUBTITLE} terms={strings.stringsSignup.TERMS}>
                <div className={"signinContainer"}>
                    <Row>
                        <Col xs={12} lg={6} xl={3} className={"marginCenter"}>
                            <Row>
                                <Col xs={12} className={"marginTop20"}>
                                    <Input error={this.state.errors.email} label={strings.stringsSignup.LBL_EMAIL}
                                           onChange={utils.onInputChangeListener.bind(this, "email")}/>
                                </Col>
                                <Col xs={12} className={"marginTop20"}>
                                    <Input type={"password"} error={this.state.errors.password} label={strings.stringsSignup.LBL_PASSWORD}
                                           onChange={utils.onInputChangeListener.bind(this, "psw1")}/>
                                </Col>
                                <Col xs={12} className={"marginTop20"}>
                                    <Input type={"password"} error={this.state.errors.password} label={strings.stringsSignup.LBL_PASSWORD_2}
                                           onChange={utils.onInputChangeListener.bind(this, "psw2")}/>
                                </Col>

                                <Col xs={12} className={"marginTop20"}>
                                    <p className={"error"}>{this.state.errors.message}</p>
                                    <p className={"success"}>{this.state.successMessage}</p>
                                </Col>
                                <Col xs={12} className={"marginTop20 center"}>
                                    <Link to={constants.NAVIGATION_SIGNIN}>{strings.stringsSignup.LBL_GO_TO_SIGNIN}</Link>
                                </Col>
                                <Col xs={12} className={"marginTop20"}>
                                    <AsyncButton className={"defaultButton"}
                                                 loading={this.state.loading}
                                                 textButton={strings.stringsSignup.BTN_SUBMIT}
                                                 onClick={this.signupHandler}/>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </div>
            </LoginContainer>
        );
    }
}

export default Signup;