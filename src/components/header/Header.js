import React, {Component} from 'react';
import * as css from './Header.css';
import {Col, Row} from "react-flexbox-grid";
import {Link} from "react-router-dom";
import * as strings from "../../utils/Strings";
import * as constants from "../../utils/Constants";
import {
    Collapse,
    Nav,
    Navbar,
    NavbarBrand,
    MenuItem,
    NavItem,
    NavDropdown,
    FormGroup,
    FormControl
} from "react-bootstrap";


const Header = (props) => {
    return (
        <div className={"headerContainer"}>
            <Navbar className={"headerFE navbar-fixed-top" + (props.blurred ? " blur" : "")} fluid style={props.style}>
                <Navbar.Header>
                    <Navbar.Toggle/>
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav>
                        <NavItem eventKey={3}>
                            <Link to={constants.NAVIGATION_HOME} className="Nav__brand">
                                {strings.stringsHomeContainer.NAVBAR_HOME}
                            </Link>
                        </NavItem>
                        <NavItem eventKey={3}>
                            <Link to={constants.NAVIGATION_GRAIN_LIST} className="Nav__brand">
                                {strings.stringsHomeContainer.NAVBAR_GRAIN}
                            </Link>
                        </NavItem>
                        <NavItem eventKey={3}>
                            <Link to={constants.NAVIGATION_HOME} className="Nav__brand">
                                {strings.stringsHomeContainer.NAVBAR_HOPS}
                            </Link>
                        </NavItem>
                        <NavItem eventKey={3}>
                            <Link to={constants.NAVIGATION_HOME} className="Nav__brand">
                                {strings.stringsHomeContainer.NAVBAR_RECIPES}
                            </Link>
                        </NavItem>
                        <NavItem eventKey={3}>
                            <Link to={constants.NAVIGATION_SIGNIN} className="Nav__brand">
                                {strings.stringsHomeContainer.NAVBAR_SIGNUP}
                            </Link>
                        </NavItem>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>


        </div>
    );
};


export default Header;