import React from 'react';
import * as css from './Separator.css';

const Separator = (props) => {
    return (
        <div className={"separatorContainer"} style={{width:props.width, height:props.height, marginTop:props.marginTop}}/>
    );
};

export default Separator;
