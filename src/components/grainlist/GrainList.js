import React, {Component} from 'react';
import MainContainer from "../maincontainer/MainContainer";
import * as strings from "../../utils/Strings";

class GrainList extends Component {
    constructor(args) {
        super(args);
        this.state = {
            grains:[]
        }
    }

    componentDidMount() {

    }

    render() {
        return (
            <MainContainer>
                <div className={"grainListContainer"}>
                    <h1 className={"title center"}>
                        {strings.stringsGrains.TITLE}
                    </h1>
                </div>
            </MainContainer>
        );
    }
}

export default GrainList;