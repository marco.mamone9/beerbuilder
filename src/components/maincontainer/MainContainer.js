import React from 'react';
import * as css from './MainContainer.css';
import Header from "../header/Header";

const MainContainer = (props) => {
    return (
        <div className={"mainContainer"}>
            <Header/>
            <div className={"child"}>
                {props.children}
            </div>
        </div>
    );
};
export default MainContainer;
