import React, {Component} from 'react';
import css from './Input.css';
class Input extends Component {
    constructor(args) {
        super(args);
        this.state = {}
    }

    render() {
        return (
            <div style={{position: "relative"}}>
                <div className={"customInput"} style={this.props.style}>
                    <div className={"customLabel"}>{this.props.label}</div>
                    <input
                        type={this.props.type ? this.props.type : "text"}
                        onChange={this.props.onChange}
                        readOnly={this.props.readOnly}
                        disabled={this.props.disabled}
                        placeholder={this.props.placeholder}
                        value={this.props.value}
                        name={this.props.name}
                        className={(this.props.className ? this.props.className : "defaultInput") + " " + (this.props.error ? "defaultInputError" : "")}/>
                </div>
            </div>
        )
    }
}
export default Input;