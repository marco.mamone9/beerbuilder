import {  toast } from 'react-toastify';

export function logError(error){
    try{
        console.log("Error:",JSON.stringify(error), "caller:", logError.caller);
    }catch(e){
        try{
            console.log("Error:",JSON.stringify(error));
        }catch (e2) { }
    }
}

export function deepCopy(obj) {
    var copy;

    // Handle the 3 simple types, and null or undefined
    if (null == obj || "object" != typeof obj) return obj;

    // Handle Date
    if (obj instanceof Date) {
        copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    if (obj._isAMomentObject) {
        copy = obj.clone();
        return copy;
    }

    // Handle Array
    if (obj instanceof Array) {
        copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = deepCopy(obj[i]);
        }
        return copy;
    }

    // Handle Object
    if (obj instanceof Object) {
        copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = deepCopy(obj[attr]);
        }
        return copy;
    }

    throw new Error("Unable to copy obj! Its type isn't supported.");
}

export function validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

export function isBlank(str){
    if(!str || str.trim()==="") return true;
    return false;
}


export function showSuccess(message){
    toast.success(message);
    window.scrollTo(0, 0);
}
export function showError(message){
    toast.error(message);
    window.scrollTo(0, 0);
}

export function onInputChangeListener(field, input ){
    if(field){
        const tempState = deepCopy(this.state);
        tempState[field] = input.target.value;
        this.setState(tempState);
    }else{
        console.log("field not valorized");
    }
}