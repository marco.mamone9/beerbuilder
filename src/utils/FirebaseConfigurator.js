import firebase from 'firebase'
let projectId = '';

if (process.env.REACT_APP_ENV === 'FAKE->TODO') {
    const prodConfig = {
        apiKey: "AIzaSyCNCeQgLdz2hLHUgAHwGFY3yFA0-rl62h0",
        authDomain: "openforce-production.firebaseapp.com",
        databaseURL: "https://openforce-production.firebaseio.com",
        projectId: "openforce-production",
        storageBucket: "openforce-production.appspot.com",
        messagingSenderId: "252074354630"
    };
    projectId = 'openforce-production';
    if (!firebase.apps.length) {
        firebase.initializeApp(prodConfig);
    }
} else {
    const stagingConfig = {
        apiKey: "AIzaSyAXMloVPlCKvB0Q2ky937xFaQt7nv0hHas",
        authDomain: "beerbuilder-faba6.firebaseapp.com",
        databaseURL: "https://beerbuilder-faba6.firebaseio.com",
        projectId: "beerbuilder-faba6",
        storageBucket: "beerbuilder-faba6.appspot.com",
        messagingSenderId: "847734139892"
    }
    projectId = 'beerbuilder-faba6';
    if (!firebase.apps.length) {
        firebase.initializeApp(stagingConfig);
    }
}



export const ProjectId = projectId;
export const FirebaseRef = firebase.firestore();
export const FirebaseFunctionsRef = firebase.functions();

const settings = {timestampsInSnapshots: true};
FirebaseRef.settings(settings);

export const FirebaseAuth = firebase.auth