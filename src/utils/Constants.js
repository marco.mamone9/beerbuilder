export const NAVIGATION_HOME = "/";
export const NAVIGATION_GRAIN_LIST = "/grains";
export const NAVIGATION_SIGNIN = "/signin";
export const NAVIGATION_SIGNUP = "/signup";


export const USER_CLUSTER_NORMAL = "normal";
export const USER_CLUSTER_TEAM_LEADER = "team_leader";