import LocalizedStrings from 'react-localization';

//stringsHomeContainer:
export const stringsHomeContainer = new LocalizedStrings({
    en: {
        NAVBAR_HOME:"Home",
        NAVBAR_GRAIN:"Grains",
        NAVBAR_HOPS:"Hops",
        NAVBAR_RECIPES:"Recipes",
        NAVBAR_SIGNUP:"Login/Register",
        NAVBAR_PROFILE:"Profile"
    }
});

//Signin
export const stringsSignin = new LocalizedStrings({
    en: {
        TITLE:"Sign In",
        SUBTITLE:"",
        TERMS:"By logging in, you agree to our terms of service.",
        LBL_EMAIL:"Email",
        LBL_PASSWORD:"Password",
        BTN_SUBMIT:"Submit",
        LBL_GO_TO_SIGNUP:"Don't have an account yet? Signup here"
    }
});

//Signup
export const stringsSignup = new LocalizedStrings({
    en: {
        TITLE:"Sign Up",
        SUBTITLE:"",
        LBL_EMAIL:"Email",
        LBL_PASSWORD:"Password",
        LBL_PASSWORD_2:"Confirm Password",
        BTN_SUBMIT:"Submit",
        LBL_GO_TO_SIGNIN:"Do you have already an account? Signin here",
        MESSAGE_SUCCESS_SIGNUP:"An email was sent to the address. Please confirm before you signin.",
        errors:{
            ERROR_EMAIL:"Invalid email",
            ERROR_PASSWORD:"Invalid password. The password must be at least 6 chars",
            ERROR_PASSWORD_MATCH:"The two password doesn't match",
        },
    }
});

//GrainList
export const stringsGrains = new LocalizedStrings({
    en: {
        TITLE:"Grains",
    }
});

