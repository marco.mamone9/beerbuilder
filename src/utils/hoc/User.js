export default class User{
    constructor(_user) {
        if(_user){
            this.uid = _user.uid;
            this.email = _user.email;
            this.cluster = _user.cluster;
            this.profileImage = _user.profileImage?_user.profileImage:null;
            this.teams = _user.teams?_user.teams:[];
        }
    }

    static getInstance = (user) =>{
        user = user || null;
        if(!user){
            return undefined;
        }
        return new User(user);;
    }
}